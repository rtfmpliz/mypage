---
title: "Catatan C#"
date: 2021-03-14T20:03:41+07:00
draft: false
---

# Catatan C#

[toc]

# Tips

## Publish / Debug

### using other than port 5000

https://stackoverflow.com/a/37365382/5038927

In ASP.NET Core 3.1, there are 4 main ways to specify a custom port:

- Using command line arguments, by starting your .NET application with `--urls=[url]`:

```
dotnet run --urls=http://localhost:5001/
```

- Using `appsettings.json`, by adding a `Urls` node:

```
{
  "Urls": "http://localhost:5001"
}
```

- Using environment variables, with `ASPNETCORE_URLS=http://localhost:5001/`.
- Using `UseUrls()`, if you prefer doing it programmatically:

```cs
public static class Program
{
    public static void Main(string[] args) =>
        CreateHostBuilder(args).Build().Run();

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(builder =>
            {
                builder.UseStartup<Startup>();
                builder.UseUrls("http://localhost:5001/");
            });
}
```

Or, if you're still using the web host builder instead of the generic host builder:

```cs
public class Program
{
    public static void Main(string[] args) =>
        new WebHostBuilder()
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseIISIntegration()
            .UseStartup<Startup>()
            .UseUrls("http://localhost:5001/")
            .Build()
            .Run();
}
```

# EFCore 

## Multiple Database 

create , 

- 2 connection di appsettings.json, 

- 2 DbContext

- masukkan dalam startup juga

  

untuk migrasi DB beri tambahan -Context atau --context

`Add-Migration init -Context PartsDbContext`

`Update-Database -Context PartsDbContext`

```cs
dotnet ef migrations add InitialCreate --context BlogContext
dotnet ef database update
```

https://stackoverflow.com/questions/43767933/entity-framework-core-using-multiple-dbcontexts

https://www.codeproject.com/Articles/1237253/Entity-Framework-Core-with-multiple-DB-Contexts-Sc

## Update Dotnet EF

`dotnet tools update --global dotnet-ef`

## Migrasi

Error pada saat migrasi 

1. Tambahkan `--verbose` untuk melihat error log
2. Jika ada 2 atau lebih **DbContext** maka sertakan pilihan DbContext dengan cara `--context [Nama DbContext]`

## Cara Reverse Engineering DbContext

1. create directory misal `mkdir c:\dev\efcore`
2. pindah ke dir yang di buat dengan `cd c:\dev\efcore`
3. create new project misal `dotnet new mvc` lalu restore package dengan `dotnet restore`

### SQL Server

4. tambahkan package dengan nuget  :

   1. Microsoft.EntityFrameCore
   2. Microsoft.EntityFrameCore.SqlServer
   3. Microsoft.EntityFrameCore.Tools
   4. Microsoft.EntityFrameCore.Design

   Jangan lupa **build**

5. atau bisa dengan CLI 

   ```
   dotnet add package Microsoft.EntityFrameworkCore.Tools
   dotnet add package Microsoft.EntityFrameworkCore.Design
   dotnet add package Microsoft.EntityFrameworkCore.SqlServer
   ```

6. buat context dengan

   ```
   dotnet ef dbcontext scaffold "Data Source=.;Initial Catalog=DBKaryawan;User ID=sa;Password=P@ssw0rd" Microsoft.EntityFrameWorkCore.SqlServer --context-dir Data --output-dir Models --verbose
   ```

### Postgre SQL

tambahkan package : 

1. Npgsql.EntityFrameworkCore.PostgreSQL
2. Npgsql.EntityFrameworkCore.PostgreSQL.Design

Jangan lupa **build**

```
dotnet ef dbcontext scaffold "Host=localhost; Port=5432; Database=suratk3b; Username=postgres; Password=P@ssw0rd" Npgsql.EntityFrameworkCore.PostgreSQL --context-dir Data --output-dir ModelPGs --verbose
```

//force using Data Annotaion

```
dotnet ef dbcontext scaffold "Host=localhost; Port=5432; Database=test_istw; Username=postgres; Password=P@ssw0rd" Npgsql.EntityFrameworkCore.PostgreSQL --context-dir DataAnnots --output-dir ModelTestISTWDataAnnots --verbose --data-annotations
```

```
dotnet ef dbcontext scaffold "Host=localhost;Port=5432;Database=test_istw; Username=postgres; Password=P@ssw0rd" Npgsql.EntityFrameworkCore.PostgreSQL --context-dir Data/DataAnnot --output-dir Models/ModelPGSQL_test_istw_annot --verbose --data-annotations
```



## Cara mengetahui .net yg terinstall

ketika `dotnet --info` pada command prompt

![image-20200719181448046](../../../../../Gdrivertfmpliz16/AppData/Roaming/Typora/typora-user-images/image-20200719181448046.png)

# Cara tambah

## Split Project Migration

1. Buat 1 Project misal MVC `SplitMigration`

   1. Tambahkan package

      ```c#
      Microsoft.EntityFrameworkCore.SqlServer
      Microsoft.EntityFrameworkCore.Design
      Microsoft.EntityFrameworkCore.Tools
      ```

   2. Tambahkan ConnectionString pada appsetting.json

      ```json
        "AllowedHosts": "*",
        "ConnectionString": {
          "DefaultConnection": "Server=.;Database=test-pemahaman-splitmigration;User Id=sa;Password=P@ssw0rd"
        }
      ```

      

2. Tambahkan Project Class Library `SplitMigrationData`

   1. Tambahan Folder Entities dan Class Person

      ```c#
          public class Person 
          {
              public string Name { get; set; }
          }
      ```

      

   2. Tambahkan Folder Data dan Class MyDbContext

      ```c#
          public class MyDbContext : DbContext
          {
              public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
              {
      
              }
      
              public DbSet<Person> People { get; set; }
          }
      ```

   3. Add package **Microsoft.EntityFrameWorkCore**

3. pada project MVC `SplitMigration` Add Reference ke Class Library `SplitMigrationData`

4. pada `SplitMigration.Startup` tambahkan

   ```c#
               var connectionString = Configuration["ConnectionString:DefaultConnection"];
               services.AddDbContext<MyDbContext>(
                   options => options.UseSqlServer(connectionString,
                   b => b.MigrationsAssembly("SplitMigration")));
   ```

5. Build Project lalu pada CLI masuk ke folder SplitMigration, jalankan perintah berikut :

   ```
   dotnet ef migrations add Initial
   dotnet ef database update
   
   ```

### Catatan

lokasi appsetttings.json dan DbContext tidak berpengaruh  apakah berada di Main Project / di Class Library, selama sudah di state di dalam Startup.cs

## AuditColumn

artikel dari : 

https://gavilan.blog/2019/08/04/audit-by-columns-with-entity-framework-core/

https://youtu.be/eLH566y6wdw

https://github.com/gavilanch/AuditByColumns/tree/master/eng/AuditEFCore

1. Buat Class dengan Entity

   ```c#
   public abstract class Entity
   {
       public int Id { get; set; }
       public string CreatedByUser { get; set; }
       public DateTimeOffset CreatedDate { get; set; }
       public string ModifiedByUser { get; set; }
       public DateTimeOffset ModifiedDate { get; set; }
   }
   ```

2. Buat Class **Person.cs** turunkan dari class **Entity.cs**

   ```c#
   public class Person : Entity
   {
   	public string Name { get; set; }
   }
   ```

3. Scaffold Controller dan Views dengan Dbcontext

   jika perlu ubah DbContext menjadi , jangan lupa ubah juga pada method di dalam PeopleController

   ```c#
   public DbSet<AuditEFCore2.Models.Person> People { get; set; }
   ```

4. Buat migrasion dengan perintah 

   ```
   dotnet-f migrations Add People
   dotnet-ef database update 
   ```

5. Tambahkan link pada menu layout untuk akses ke page People

   ```c#
   <li class="nav-item">
   	<a class="nav-link text-dark" asp-area="" asp-controller="People" asp-action="Index">People</a>
   </li>
   ```

6. Hapus TextBox pada views **Create & Update**

   Padatahap ini kitasudah bisa create dan update pada views , tapi harus input secara manual User dan CreationDate dll.

7. override method SaveChangesAsync pada DbContext sehingga ketika panggil perintah Save ada data yg ditambhakn secara otomatis

   ```c#
   public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
   {
   	ProcessSave();
       return base.SaveChangesAsync(cancellationToken);
   }
   
   private void ProcessSave()
   	{
       	var currentTime = DateTimeOffset.UtcNow;
           foreach (var item in ChangeTracker.Entries()
           .Where(e => e.State == EntityState.Added && e.Entity is Entity))
   	{
       	var entidad = item.Entity as Entity;
           entidad.CreatedDate = currentTime;
           entidad.CreatedByUser = "";
           entidad.ModifiedDate = currentTime;
           entidad.ModifiedByUser = "";
   	}
   
       foreach (var item in ChangeTracker.Entries()
       .Where(e => e.State == EntityState.Modified && e.Entity is Entity))
       {
       	var entidad = item.Entity as Entity;
           entidad.ModifiedDate = currentTime;
           entidad.ModifiedByUser = "";
   		item.Property(nameof(entidad.CreatedDate)).IsModified = false;
           item.Property(nameof(entidad.CreatedByUser)).IsModified = false;
   	}
   }
   ```

   

8. Buat Interface dan  pada folder Helpers untuk ambi Username

   ```c#
       public interface ICurrentUserService
       {
           string GetCurrentUsername();
       }
   ```

   ```c#
   public class CurrentUserService : ICurrentUserService
   {
   	private readonly IHttpContextAccessor httpContextAccessor;
   	
       public CurrentUserService(IHttpContextAccessor httpContextAccessor)
       {
       	this.httpContextAccessor = httpContextAccessor;
       }
       
       public string GetCurrentUsername()
       {
       	return httpContextAccessor.HttpContext.User.Identity.Name;
   	}
   }
   ```

   

9. Tambahan Interface dan Class pada **Startup.cs**

   ```c#
   services.AddTransient<ICurrentUserService, CurrentUserService>();
   ```

   

10. Inject **ICurrentUserService ke DbContext**

   ```c#
   public AuditEFCore2Context(
       DbContextOptions<AuditEFCore2Context> options,
       ICurrentUserService currentUserService): base(options)
   {
       this.currentUserService = currentUserService;
   }
   ```

11. masukkan juga dalam method ProcessSave()

    ```c#
    entidad.CreatedByUser = currentUserService.GetCurrentUsername();
    entidad.ModifiedByUser = currentUserService.GetCurrentUsername();
    ```

    

## AutoMapper

```c#
services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
```

Tambahkan Folder **Profiles** dan class **AuthorProfile.cs**

```c#
using AutoMapper;
using CourseLibrary.API.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseLibrary.API.Profiles
{
    public class AuthorsProfile : Profile
    {
        public AuthorsProfile()
        {
            CreateMap<Entities.Author, Models.AuthorDto>()
                .ForMember(
                    dest => dest.Name,
                    opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(
                    dest => dest.Age,
                    opt => opt.MapFrom(src => src.DateOfBirth.GetCurrentAge())
                );
        }
    }
}
```

Create AuthorDto pada Models

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseLibrary.API.Models
{
    public class AuthorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string MainCategory { get; set; }
    }
}

```

Ubah AuthorsController

```c#
using AutoMapper;
using CourseLibrary.API.Helpers;
using CourseLibrary.API.Models;
using CourseLibrary.API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseLibrary.API.Controllers
{
    [ApiController]
    [Route("api/{controller}")]

    public class AuthorsController : ControllerBase
    {
        private readonly ICourseLibraryRepository _courseLibraryRepository;
        private readonly IMapper _mapper;

        public AuthorsController(ICourseLibraryRepository courseLibraryRepository,
            IMapper mapper)
        {
            _courseLibraryRepository = courseLibraryRepository;
            _mapper = mapper;
        }
        public ActionResult<IEnumerable<AuthorDto>> GetAuthors()
        {
            var authorsFromRepo = _courseLibraryRepository.GetAuthors();
            return Ok(_mapper.Map<IEnumerable<AuthorDto>>(authorsFromRepo));
        }
        [HttpGet("{authorId}")]
        public IActionResult GetAuthor(Guid authorId)
        {
            var authorFromRepo = _courseLibraryRepository.GetAuthor(authorId);
            if (authorFromRepo == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<AuthorDto>(authorFromRepo));


        }
    }
}

```







## Swagger

Install **SwashBuckle**

tambahkan pada Method **ConfigureServices** di **Startup.cs**

```c#
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "My API",
                    Version = "v1"
                });
                c.IncludeXmlComments(GetXmlCommentsPath());

            });
```

Method **Configure**

```c#
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
```

Tambahkan juga MEthod dibawah

```c#
        private string GetXmlCommentsPath()
        {
            var app = PlatformServices.Default.Application;

            //var app = System.AppContext.BaseDirectory;
            return System.IO.Path.Combine(app.ApplicationBasePath, "CourseLibrary.API.xml");
        }
```

buka **Debug** Properties Project , nama Project klik kanan Properties

![image-20200713121409343](../../../../../Gdrivertfmpliz16/AppData/Roaming/Typora/typora-user-images/image-20200713121409343.png)

dan pada **Build**



![image-20200713121524200](../../../../../Gdrivertfmpliz16/AppData/Roaming/Typora/typora-user-images/image-20200713121524200.png)

sesuaikan nama XML Documentation di method **GetXmlCommentsPath** pada **Startup.cs**



**XML cara lain**

**csproj**

```csharp
  <PropertyGroup>
    <GenerateDocumentationFile>true</GenerateDocumentationFile>
    <NoWarn>$(NoWarn);1591</NoWarn>
  </PropertyGroup>
```

**Startup.cs**

```c#
            services.AddSwaggerGen(options =>
            {

                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Conference Planner API", Version = "v1" });

                //Set the comments path for the Swagger JSON and UI
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
```



# Penggunaan

## Penggunaan ActionResult<T> dan IActionResult



```c#
public ActionResult<IEnumerable<AuthorDto>> GetAuthors()  
{
    
}  
```

**sebelumnya**

```c#
public IActionResult GetAuthors()
{
    
}
```



```c#
    public ActionResult<IEnumerable<AuthorDto>> GetAuthors()
    {
        var authorsFromRepo = _courseLibraryRepository.GetAuthors();
        var authors = new List<AuthorDto>();

        foreach (var author in authorsFromRepo)
        {
            authors.Add(new AuthorDto()
            {
                Id = author.Id,
                Name = $"{author.FirstName} {author.LastName}",
                MainCategory = author.MainCategory,
                Age = author.DateOfBirth.GetCurrentAge()

            });
        }
        return Ok(authors);
    }
```



## Model vs Entity

Gunakan Model daripada Entity

Contoh **Model**

```c#
public class AuthorDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public string MainCategory { get; set; }
}
```

Contoh **Entity**

```c#
public class Author
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        public DateTimeOffset DateOfBirth { get; set; }          

        [Required]
        [MaxLength(50)]
        public string MainCategory { get; set; }

        public ICollection<Course> Courses { get; set; }
            = new List<Course>();
}
```

# Belum di coba

# Soft Delete with EF Core

http://blog.stashev.com/soft-delete/

02 JUNE 2018

   When developing an application there are many times when you want to allow your users to delete some data that they no longer need. However you may not want to actually delete it but rather only hide it. The reasons for this can vary. You may want to allow for undoing the delete or you may be obliged to keep the data.

This kind of deletion is often called a “soft delete”. That’s because you only mark the entities as deleted and never show them again.

## Query Filters

   Starting with Entity Framework Core 2.0 they have introduced *Global Query Filters*. Here is the [article](https://docs.microsoft.com/en-us/ef/core/querying/filters) which describes them. The idea is that you setup your filter on a global level and it will be applied to every query to the database. So you can say `.Where(x => !x.IsDeleted)` in one place. Of course you can ignore the filter in specific places if you need to. That seems great but here are some things that can be done better.

## Deletable Entities

Consider the following class.

```prettyprint
public class Employee  
{
    public int Id { get; set; }
    public string Name { get; set; }
}
```

What will be needed in order to add a soft delete functionality to it? For a start we will have to add the `IsDeleted` property.

```prettyprint
public class Employee  
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsDeleted { get; set; }
}
```

Then we add a migration for the new property. And we add the global query filter.

```prettyprint
protected override void OnModelCreating(ModelBuilder builder)  
{
    base.OnModelCreating(builder);
    // ...
    ConfigureDeletableEntities(builder);
}

private void ConfigureDeletableEntities(ModelBuilder modelBuilder)  
{
    modelBuilder.Entity<Employee>().HasQueryFilter(x => !x.IsDeleted);
    // ...
}
```

And when deleting the entity we will have to set the `IsDeleted` property instead of actually deleting it. In our generic `Repository<T>` we may have something like

```prettyprint
public virtual void Delete(T entity)  
{
    switch (entity)
    {
        case Employee employee:
            employee.IsDeleted = true;
            break;

        // ...

        default:
            _mainContext.Remove(entity);
            break;
    }
}
```

### Adding an interface

It will be better if we had an interface for the property.

```prettyprint
public interface IDeletableEntity  
{
    bool IsDeleted { get; set; }
}

public class Employee : IDeletableEntity  
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsDeleted { get; set; }
}
```

Then the repository will look like

```prettyprint
public virtual void Delete(T entity)  
{
    if (entity is IDeletableEntity deletable)
    {
        deletable.IsDeleted = true;
    }
    else
    {
        _mainContext.Remove(entity);
    }
}
```

## Removing the Property

   All this seems fine and it will work. The only thing that bothers me is that if you are going to use the `Employee` class as a model throughout your business logic the `IsDeleted` property should not be there. First of all you will not filter on it because it is already filtered on a global level. Second you shouldn't be able to modify it. After all if you want to delete the entity you should use the repository. Actually you can make it read-only but this will fix only the second half of the problem. Let's try to remove it altogether.

We remove the interface and the property and we are left with the clean `Employee` class.

```prettyprint
public class Employee  
{
    public int Id { get; set; }
    public string Name { get; set; }
}
```

We now have four problems to solve:

- We have to recognize which entities will support the *soft delete* functionality.
- We have to add the `IsDeleted` property in the database for them.
- We have to implement the soft deletion in the repository without using the strongly typed property.
- We have to add the global query filter for all entities that support soft deletion without using the strongly typed property.

#### Soft Delete Functionality

When we had the interface it was easy to know if a class was an `IDeletableEntity`. Now that we don't have it we can create the following extension method

```cs
public static class AuditUtils
{
    public const string IsDeletedColumnName = "IsDeleted";

    private static readonly IEnumerable<Type> _deletableTypes = new[]
    {
        typeof(Employee)
        // ...
    };

    public static bool SupportsSoftDeletion(this Type type)
    {
        return _deletableTypes.Contains(type);
    }
}
```

Note that I've added also the `IsDeletedColumnName` which we will need later.

#### Adding the Property

We will now need the property in the database. Also we should be able to create a migration to add the column to the database. This can be achieved with the `ModelBuilder` like so

```prettyprint
protected override void OnModelCreating(ModelBuilder builder)  
{
    base.OnModelCreating(builder);
    // ...
    ConfigureDeletableEntities(builder);
}

private void ConfigureDeletableEntities(ModelBuilder modelBuilder)  
{
    foreach (var entity in modelBuilder.Model.GetEntityTypes())
    {
        if (entity.ClrType.SupportsSoftDeletion())
        {
            entity.AddProperty(AuditUtils.IsDeletedColumnName, typeof(bool));
        }
    }
}
```

#### The Delete in the Repository

Now we have to implement the soft delete in the repository. We don't have the boolean property, but Entity Framework lets us access columns by their names like so

```prettyprint
public virtual void Delete(T entity)  
{
    if (typeof(T).SupportsSoftDeletion())
    {
        var oldEntity = _mainContext.Find<T>(entity.Id);
        var isDeletedProperty = _mainContext
            .Entry(oldEntity)
            .Property(AuditUtils.IsDeletedColumnName);

        isDeletedProperty.CurrentValue = true;
    }
    else
    {
        _mainContext.Remove(entity);
    }
}
```

#### The Global Query Filter

We can add the filter in the same place where we added the property.

```cs
private void ConfigureDeletableEntities(ModelBuilder modelBuilder)
{
    foreach (var entity in modelBuilder.Model.GetEntityTypes())
    {
        if (entity.ClrType.SupportsSoftDeletion())
        {
            entity.AddProperty(AuditUtils.IsDeletedColumnName, typeof(bool));

            modelBuilder
                .Entity(entity.ClrType)
                .HasQueryFilter(GetIsDeletedRestriction(entity.ClrType));
        }
    }
}
```

This part is a bit tricky because it actually expects a *lambda expression*. When we had the property we could use the lambda function `x => !x.IsDeleted` which can be implicitly converted to a lambda expression. Now we'll have to construct it manually.

```prettyprint
private LambdaExpression GetIsDeletedRestriction(Type type)  
{
    var propMethod = typeof(EF)
        .GetMethod(nameof(EF.Property), BindingFlags.Static | BindingFlags.Public)
        .MakeGenericMethod(typeof(bool));

    var param = Expression.Parameter(type, "it");
    var columnName = Expression.Constant(AuditUtils.IsDeletedColumnName);
    var prop = Expression.Call(propMethod, param, columnName);
    var falseConst = Expression.Constant(false);
    var condition = Expression.MakeBinary(ExpressionType.Equal, prop, falseConst);
    return Expression.Lambda(condition, param);
}
```

## The Result

   At the end it seems like this solution is more complicated than what we started with. But it is implemented only once, on a global level. And it has the following two advantages: it can be used extremely easy for another entity and the entity doesn't have to know about it (no IsDeleted property). If we want to enable the functionality for another type we just have to add it to the list like so

```cs
private static readonly IEnumerable _deletableTypes = new[]
{
    typeof(Employee),
    typeof(Customer)
};
```

And that's all. Nothing else is required.

## Unique Indexes

   One last thing. In some of the tables we will have unique indexes. Most of the time they will have to allow duplication for the *soft deleted* entities. This can be achieved in SQL Server with a *filtered index* like so

```c#
create unique index IX_Employees_DomainUserName  
  on Employees (DomainUserName)
  where (IsDeleted = 0);
```

# Structuring UPDATE and DELETE queries so that they're a little bit safer

Wednesday, May 30, 2018[sql server](https://robertwray.co.uk/Tags/sql server), [sql](https://robertwray.co.uk/Tags/sql)

![Always practice safe querying!](my-first-post.assets/sql_server_safe_querying.PNG)

Quite often I've seen someone who wants to update a table, or delete rows from a table write a SELECT query to work out what data they want then separately write an UPDATE or DELETE query to perform the actual task. In the **best** scenarios where I've seen that happen, the WHERE has been copied and pasted across, in the (nearly) **worst** it's been written again. In the **very worst** the second query that was having the UPDATE peformed against it was actually performed against a different table that happened to contain columns with the same names as those specified in the WHERE (thanks for that, Connor!).

There is a safer way to write queries when you're trying to determine what rows you want to affect, which is to swap this:

```
SELECT	*
FROM	[dbo].[User]
WHERE	LockedOut = 0
AND	ManagerUserId = 3

DELETE	[dbo].[User]
WHERE	LockedOut = 0
AND	ManagerUserId = 3
```

For this:

```
SELECT	*
-- DELETE U
FROM	[dbo].[User] U
WHERE	U.LockedOut = 0
AND	U.ManagerUserId = 3
```

By structuring the query in this way and then swapping the **SELECT \*** for the **DELETE** or **UPDATE** the likelihood of making a mistake is much reduced as the same query is effectively being used to determine what data to change and to carry out the change.

Comment out the SELECT, un-comment the DELETE, press F5 and it's job done, safely.



## Entity Framework

### 1. Get Last ID

```c#
db.Users.OrderByDescending(u => u.UserId).FirstOrDefault();
```

https://stackoverflow.com/questions/315946/how-do-i-get-the-max-id-with-linq-to-entity

```cs
int intIdt = db.Users.Max(u => u.UserId);
```

**Update:**

If no record then generate exception using above code try this

```cs
int? intIdt = db.Users.Max(u => (int?)u.UserId);
```

```c#
Users.OrderByDescending(x=>x.Id.Length).ThenByDescending(a => a.Id).FirstOrDefault();
```

```c#
Users user = bd.Users.Where(u=> u.UserAge > 21).Max(u => u.UserID); 
```

```c#
var max = db.Users.DefaultIfEmpty().Max(r => r == null ? 0 : r.ModelID);
```

